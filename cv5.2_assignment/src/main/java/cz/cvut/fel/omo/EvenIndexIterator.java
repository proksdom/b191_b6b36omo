package cz.cvut.fel.omo;

import java.util.NoSuchElementException;

public class EvenIndexIterator implements Iterator {

    int [] array;
    int currentIndex = 0;

    public EvenIndexIterator(int [] array){
        this.array = array;
    }

    public int currentItem(){
        if (isEmpty())
            throw new NoSuchElementException();
        return array[currentIndex];
    }

    public int next(){
        if (currentIndex+2 < array.length) {
            currentIndex += 2;
            return array[currentIndex];
        }
        throw new NoSuchElementException();
    }

    public boolean isDone(){
        return array.length - currentIndex <= 2;
    }

    public int first(){
        currentIndex = 0;
        return array[currentIndex];
    }

    private boolean isEmpty(){
        return array.length == 0;
    }
}
