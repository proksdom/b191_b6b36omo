package cz.cvut.fel.omo.blog;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class TopicTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private Topic topic;

    @Before
    public void executeBeforeEach(){
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        topic = new Topic("AngularJS", "Cachy content");
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

    @Test
    public void displayComponent_displayValidComponent_topicDisplayedAccordingly() {
        // arrange
        String topicName = topic.getTopicName();
        String topicDescription = topic.getTopicDescription();
        String expectedString = topicName + ": " + topicDescription + "\n";
        // act
        topic.displayComponent();
        // assert
        assertEquals(expectedString, outContent.toString());
    }

    @Test
    public void registerPost_registerTopicToPost_postRegisteredUnderTopic() {
        // arrange
        Post post = new Post("postHeader", "postContent", null);
        boolean expectedValue = true;
        // act
        topic.registerPost(post);
        boolean actualValue = topic.getTopicPosts().contains(post);
        // assert
        assertEquals(expectedValue, actualValue);
    }


}