package cz.cvut.fel.omo.blog;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BlogTest {

    private Blog blog;
    private String existingUsername;
    private String existingPassword;
    private String existingTopicName;
    private String existingPostName;

    @Before
    public void executeBeforeEach(){
        blog = new Blog();
        // arrange blog user
        existingUsername = "Tom_Hanks";
        existingPassword = "password";
        blog.createNewAccount(existingUsername,existingPassword, true);
        // arrange topic
        existingTopicName = "AngularJS";
        blog.submitNewTopic(existingTopicName, "This topic contains all posts related to topic of AngularJS also known as Angular 1.");
        // arrange post
        existingPostName = "Angular JS for reusable web components.";
        blog.submitNewPost(existingPostName, "Angluar is a great for creating reusable web components", null);
    }

    @Test
    public void createNewUser_insertingUniqueUser_userAccountCollectionSizeIncreased(){
        // arrange
        int expectedValue = blog.getBlogAccounts().size() + 1;
        // act
        blog.createNewAccount("Steven_Segal", "password", true);
        // assert
        assertEquals(expectedValue, blog.getBlogAccounts().size());
    }

    @Test
    public void createNewUser_insertDuplicatedUser_userAccountCollectionSizeUnchanged(){
        // arrange
        int expectedValue = blog.getBlogAccounts().size();
        // act
        blog.createNewAccount(existingUsername, existingPassword, true);
        // assert
        assertEquals(expectedValue, blog.getBlogAccounts().size());
    }

    @Test
    public void existsUserAccount_searchExistingUser_returnsTrue(){
        // arrange
        boolean expectedValue = true;
        // act
        boolean actualValue = blog.existsUserAccount(existingUsername);
        // assert
        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void existsUserAccount_searchNotExistingUser_returnsFalse(){
        // arrange
        boolean expectedValue = false;
        // act
        boolean actualValue = blog.existsUserAccount("Steven_Segal");
        // assert
        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void login_loginExistingUser_userReturned(){
        // arrange
        BlogAccount expectedObject = blog.getBlogAccounts()
                .stream()
                .filter(userAccount -> userAccount.getUsername().equals(existingUsername))
                .filter(userAccount -> userAccount.getPassword().equals(existingPassword))
                .findFirst()
                .orElse(null);
        // act
        BlogAccount actualObject = blog.login(existingUsername, existingPassword);
        // assert
        assertEquals(expectedObject, actualObject);
    }

    @Test
    public void login_loginNonExistingUser_returnedNull(){
        // arrange
        UserAccount expectedObject = null;
        // act
        BlogAccount actualObject = blog.login("Steven_Segal", "password");
        // assert
        assertEquals(expectedObject, actualObject);
    }

    @Test
    public void login_loginWithWrongPassword_returnedNull(){
        // arrange
        BlogAccount expectedObject = null;
        // act
        BlogAccount actualObject = blog.login(existingUsername, "wrongPassword");
        // assert
        assertEquals(expectedObject, actualObject);
    }

    @Test
    public void login_loginAsBlockedUser_returnedNull(){
        // arrange
        blog.createNewAccount("blocked_user","password", false);
        blog.blockUserAccount("blocked_user");
        BlogAccount expectedObject = null;
        // act
        BlogAccount actualObject = blog.login("blocked_user", "password");
        // assert
        assertEquals(expectedObject, actualObject);
    }


    @Test
    public void createNewAccount_setAdminFlag_newlyCreatedAccountIsAdminAccount(){
        // arrange
        AdminAccount expectedClass = new AdminAccount("username", "password", null);
        boolean admin = true;
        String username = "Nicolas_Fleming";
        // act
        blog.createNewAccount(username, "password", admin);
        // assert
        assertEquals(expectedClass.getClass(), blog.getBlogAccounts()
                .stream()
                .filter(account -> account.getUsername().equals(username))
                .findFirst()
                .orElse(null).getClass());
    }

    @Test
    public void createNewAccount_unsetAdminFlag_newlyCreatedAccountIsUserAccount(){
        // arrange
        UserAccount expectedClass = new UserAccount("username", "password", null);
        boolean admin = false;
        String username = "Nicolas_Fleming";
        // act
        blog.createNewAccount(username, "password", admin);
        // assert
        assertEquals(expectedClass.getClass(), blog.getBlogAccounts()
                .stream()
                .filter(account -> account.getUsername().equals(username))
                .findFirst()
                .orElse(null).getClass());
    }

    @Test
    public void existsUserAccount_findExistingAccount_rerturnedTrue(){
        // arrange
        boolean expectedValue = true;
        // act
        boolean actualValue = blog.existsUserAccount(existingUsername);
        // assert
        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void existsUserAccount_findNonExistingAccount_returnedFalse(){
        // arrange
        String username = "Nicolas_Twisp";
        boolean expectedValue = false;
        // act
        boolean actualValue = blog.existsUserAccount(username);
        // assert
        assertEquals(expectedValue, actualValue);
    }


    @Test
    public void submitNewTopic_submitUniqueTopic_topicAmountIncreased(){
        // arrange
        int expectedAmount = blog.getBlogTopics().size() + 1;
        // act
        blog.submitNewTopic("React", "All posts regarding React framework.");
        // assert
        assertEquals(expectedAmount, blog.getBlogTopics().size());
    }

    @Test
    public void submitNewTopic_submitDuplicatedTopic_topicAmountUnchanged(){
        // arrange
        int expectedAmount = blog.getBlogTopics().size();
        // act
        blog.submitNewTopic(existingTopicName, "AngularJS best practices.");
        // assert
        assertEquals(expectedAmount, blog.getBlogAccounts().size());
    }

    @Test
    public void existsTopic_findExistingTopic_returnedTrue(){
        // arrange
        boolean expectedValue = true;
        // act
        boolean actualValue = blog.existsTopic(existingTopicName);
        // assert
        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void existsTopic_findNonExistingTopic_returnedFalse(){
        // arrange
       String nonExistingTopic = "Angular2";
        boolean expectedValue = false;
        // act
        boolean actualValue = blog.existsTopic(nonExistingTopic);
        // assert
        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void findTopic_searchExistingTopic_returnedTopic(){
        // arrange
        Topic expectedTopic = blog.getBlogTopics()
                .stream()
                .filter(topic -> topic.getTopicName().equals(existingTopicName))
                .findFirst()
                .orElse(null);
        // act
        Topic actualTopic = blog.findTopic(existingTopicName);
        // assert
        assertEquals(expectedTopic, actualTopic);
    }

    @Test
    public void findTopic_searchNonExistingTopic_returnedNull(){
        // arrange
        Topic expectedTopic = null;
        String nonExistingTopic = "React";
        // act
        Topic actualTopic = blog.findTopic(nonExistingTopic);
        // assert
        assertEquals(expectedTopic, actualTopic);
    }

    @Test
    public void submitNewPost_submitValidPost_postsAmountIncreased(){
        // arrange
        int expectedValue = blog.getBlogPosts().size() + 1;
        // act
        blog.submitNewPost("Why using angular?", "Because it is awesome.", null);
        // assert
        assertEquals(expectedValue, blog.getBlogPosts().size());
    }

    @Test
    public void findPost_findExistingPost_returnedPost(){
        // arrange
        Post expectedPost = blog.getBlogPosts()
                .stream()
                .filter(post->post.getHeader().equals(existingPostName))
                .findFirst()
                .orElse(null);
        // act
        Post actualPost = blog.findPost(existingPostName);
        // assert
        assertEquals(expectedPost, actualPost);
    }

    @Test
    public void findPost_findNonExistingPost_returnedNull(){
        // arrange
        String nonExistingPost = "Angular2";
        Post expectedPost = null;
        // act
        Post actualPost = blog.findPost(nonExistingPost);
        // assert
        assertEquals(expectedPost, actualPost);
    }
}