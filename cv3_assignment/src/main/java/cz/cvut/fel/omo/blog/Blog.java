package cz.cvut.fel.omo.blog;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Blog implements BlogInterface {

    private Dashboard dashboard = new Dashboard();
    private List<BlogAccount> blogAccounts = new ArrayList<BlogAccount>();
    private List<Topic> blogTopics = new ArrayList<Topic>();
    private List<Post> blogPosts = new ArrayList<Post>();
    private Editor editor = new Editor();

    public Blog() {
    }

    public void createNewAccount(String username, String password, boolean admin) {
        if (!existsUserAccount(username))
            if (admin)
                blogAccounts.add(new AdminAccount(username, password, this));
            else
                blogAccounts.add(new UserAccount(username, password, this));
    }

    public void blockUserAccount(String username){
        blogAccounts.stream()
                .filter(userAccount -> userAccount.getUsername().equals(username))
                .findFirst()
                .ifPresent(userAccount -> userAccount.blockAccount());
    }

    public boolean existsUserAccount(String username) {
        return blogAccounts.stream().anyMatch(userAccount -> userAccount.getUsername().equals(username));
    }

    public BlogAccount login(String username, String password) {
        return blogAccounts
                .stream()
                .filter(userAccount -> userAccount.getUsername().equals(username))
                .filter(userAccount -> userAccount.getPassword().equals(password))
                .filter(userAccount -> !userAccount.getAccountBlockage())
                .findFirst()
                .orElseGet(()->loginFailed());
    }

    public BlogAccount loginFailed(){
        Warning warning = new Warning("Login failed. Wrong username or password.");
        dashboard.display((DisplayableComponent) warning, "Warning");
        return null;
    }

    public void submitNewTopic(String topicName, String topicDescription) {
        if (!existsTopic(topicName))
            blogTopics.add(new Topic(topicName, topicDescription));
    }

    public boolean existsTopic(String topicName) {
        return blogTopics
                .stream()  
                .anyMatch(topic -> topic.getTopicName().equals(topicName));
    }

    public Topic findTopic(String topicName) {
        return blogTopics.stream()
                .filter(topic -> topic.getTopicName().equals(topicName))
                .findFirst()
                .orElse(null);
    }

    public void displayTopics() {
        dashboard.display((List<DisplayableComponent>) (List<?>) blogTopics, "Displaying topics:");
    }

    public void submitNewPost(String postHeader, String postContent, BlogAccount account) {
        if (!existsPost(postHeader))
            blogPosts.add(new Post(postHeader, postContent, account));
    }

    public Post findPost(String postHeader) {
        return blogPosts.stream()
                .filter(post -> post.getHeader().equals(postHeader))
                .findFirst()
                .orElse(null);
    }

    public boolean existsPost(String postHeader) {
        return blogPosts
                .stream()
                .anyMatch(post -> post.getHeader().equals(postHeader));
    }

    public void readBlog() {
        dashboard.display((List<DisplayableComponent>) (List<?>) blogPosts
                .stream()
                .filter(post -> post.isPublished())
                .collect(Collectors.toList()), "Displaying blog posts");
    }

    public void readBlog(String topicName) {
        Topic searchedTopic = findTopic(topicName);
        if (searchedTopic != null)
            dashboard.display((List<DisplayableComponent>) (Object) searchedTopic.getTopicPosts()
                    .stream()
                    .filter(post -> post.isPublished())
                    .collect(Collectors.toList()), "Displaying blog posts on topic of " + topicName);
    }

    public List<Post> getBlogPosts() {
        return blogPosts;
    }
    public List<BlogAccount> getBlogAccounts() {return blogAccounts; }
    public List<Topic> getBlogTopics() {
        return blogTopics;
    }

    public Editor getEditor() {
        return editor;
    }
}
