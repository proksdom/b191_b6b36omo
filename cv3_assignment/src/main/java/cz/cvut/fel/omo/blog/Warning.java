package cz.cvut.fel.omo.blog;

public class Warning implements DisplayableComponent {

    String warning;

    public Warning(String warning){
        this.warning = warning;
    }

    public void displayComponent(){
        System.out.print(warning);
    }

}
