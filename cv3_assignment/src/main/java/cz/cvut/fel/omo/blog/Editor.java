package cz.cvut.fel.omo.blog;

/*
* Editor serves as a Blog user interface for editing requested component.
*/

public class Editor {

    public void edit(EditableComponent toEdit){
        if (toEdit != null)
            toEdit.editComponent();
    }
}
