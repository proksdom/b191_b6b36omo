package cz.cvut.fel.omo.blog;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;

public class Post implements DisplayableComponent, EditableComponent {

    private String id;
    private String header;
    private String content;
    private BlogAccount author;
    private Date lastUpdated;
    private PostState state;
    private List<Topic> articleTopics = new ArrayList<>();
    private List<Comment> postComments = new ArrayList<>();

    public Post(String header, String content, BlogAccount author){
        this.id = UUID.randomUUID().toString();
        this.header = header;
        this.content = content;
        this.author = author;
        this.lastUpdated = new Date();
        this.state = PostState.INPROGRESS;
    }

    public void displayComponent(){
        if (this.state == PostState.PUBLISHED) {
            System.out.println(header);
            System.out.println(content);
            System.out.println("Author: " + author + ", last updated at " + (new SimpleDateFormat("dd-MM-yyyy").format(lastUpdated)) + "\n");
            if (postComments.size()>0) {
                System.out.println("Comments:");
                postComments.forEach(comment -> System.out.print(comment));
            }
        }
    }

    public void publishPost(){
        this.state = PostState.PUBLISHED;
    }

    public void pullOfPost(){
        this.state = PostState.OBSOLETE;
    }

    public String getHeader() {
        return header;
    }

    public void registerToTopic(Topic topic){
        articleTopics.add(topic);
        topic.registerPost(this);
    }

    public boolean isPublished(){
        return state.equals(PostState.PUBLISHED);
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public BlogAccount getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public List<Topic> getArticleTopics() {
        return articleTopics;
    }

    public void createComment(String commentContent, BlogAccount author){
        postComments.add(new Comment(commentContent, author));
    }

    public void editComponent(){
        if (wantsToChangeContent("Change post header? (Y/N)"))
            changeHeader();
        if (wantsToChangeContent("Change post content (Y/N)"))
            changeContent();
    }

    public Boolean wantsToChangeContent(String printingInstructions){
        Boolean changeHeaderOutcome = null;
        Scanner reader = new Scanner(System.in);
        while (changeHeaderOutcome == null){
            System.out.println(printingInstructions);
            String input = reader.nextLine();
            if (input.equals("Y"))
                changeHeaderOutcome = true;
            else if (input.equals("N"))
                changeHeaderOutcome = false;
            else
                System.out.print("Input not recognized\n");
        }
        return changeHeaderOutcome;
    }

    public void changeHeader(){
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter new header.\n");
        this.header = reader.nextLine();
    }

    public void changeContent(){
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter new content.\n");
        this.content = reader.nextLine();
    }

    public List<Comment> getPostComments() {
        return postComments;
    }
}
