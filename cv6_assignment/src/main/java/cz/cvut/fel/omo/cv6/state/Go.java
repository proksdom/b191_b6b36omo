package cz.cvut.fel.omo.cv6.state;

/** Kurz A7B36OMO - Objektove modelovani - Cviceni 6 Design Patterns State, strategy
 *
 *  @author mayerto1
 *
 *
 */
public class Go extends State {

    public Go(Context context){
        super(context);
        color = Color.GREEN;
        period = LightPeriod.GREEN_LIGHT_PERIOD.getValue();
    }

    @Override
    protected void changeToNextState() {
        context.setState(new Attention(context));
    }

}
