package cz.cvut.fel.omo.trackingSystem;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


/**
 *
 * @author balikm1
 */
public class TrackerTest {
    private Vehicle vehicle;
    private Tracker tracker;
    private int initialMileage;

    @Before
    public void executeBeforeEach()
    {
        initialMileage = 199049;
        vehicle = new Vehicle("Jeep", "AKSJH15615", initialMileage);
        tracker = new Tracker(0);
    }

    @Test
    public void attachTracker_vehicleNotNull_currentVehicleEqualsAttached()
    {
        // arrange
        Vehicle expectedVehicle = vehicle;

        // act
        tracker.attachTracker(vehicle);
        Vehicle actualVehicle= tracker.getCurrentVehicle();

        // assert
        assertEquals(expectedVehicle, actualVehicle);
    }

    @Test
    public void getTrackerMileage_trackerAttached_zeroMileageReturned()
    {
        // arrange
        int expectedMileage = 0;
        tracker.attachTracker(vehicle);

        // act
        int actualMileage = tracker.getTrackerMileage();

        // assert
        assertEquals(expectedMileage, actualMileage);
    }

    public void getTrackerMileage_someDistanceDriven_sumOfDrivenDistanceReturned()
    {
        // arrange
        int mileage1 = 100;
        int mileage2 = 200;
        int expectedMileage = mileage1 + mileage2;
        tracker.attachTracker(vehicle);

        // act
        vehicle.drive(mileage1);
        vehicle.drive(mileage2);
        int actualMileage = tracker.getTrackerMileage();

        // assert
        assertEquals(expectedMileage, actualMileage);
    }
}